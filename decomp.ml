(*Entrée: une liste de segments*)
(*Les points d'un segment sont donnés par x croissant*)

(*Auxilliaires*)
let find_ex c l =
	let rec aux = function
		|[] -> assert false
		|[x] -> x
		|h::t -> let x = aux t in
			if c h x then h else x in

	match l with
		|[] -> None
		|_ -> Some (aux c l)

let rec inser_pt x = function
	|[] -> [x]
	|h::t when (fst h) > (fst x) -> x::(h::t)
	|h::t when h = x -> h::t
	|h::t -> h::(inser_pt x t)

let rec points_from_segs = function
	|[] -> []
	|(a,b)::t -> inser_pt a (inser_pt b (points_from_segs t))

let rec slope s =
	let ((ax, ay), (bx, by)) = s in
	(by -. ay)/.(bx -. ax)

let clean x = match x with
	|Some y -> y
	|None -> assert false

(*Primitives*)

let prj s p =
	let ((ax, ay), (bx, by)) = s in
	match fst p with
		| x when (x >= ax) && (x <= bx) ->
			Some (x, (slope s)*.(x -. ax) +. ay)
		| _ -> None

let prjY s p =
	match prj s p with
		|Some p' -> snd p'
		|None -> None

let upP sL p =
	let aux s = match prjY s p with
		|Some y -> Some (y, p)
		|None -> None
	in
	let yL = List.filter (function (y,s) -> y > (snd p)) (List.filter_map aux sL) in

	match find_ex (function (y1, s1) (y2 s2) -> y1 < y2) yL with
		|Some (y, s) -> Some s
		|None -> None

let downP sL p =
	let aux s = match prjY s p with
		|Some y -> Some (y, p)
		|None -> None
	in
	let yL = List.filter (function (y,s) -> y < (snd p)) (List.filter_map aux sL) in

	match find_ex (function (y1, s1) (y2 s2) -> y1 > y2) yL with
		|Some (y, s) -> Some s
		|None -> None

let upS sL pL s =
	let (a,b) = s in
	let cL = b :: List.filter (function p -> downP sL p = Some s)
	in find_ex (function (x1, y1) (x2, y2) -> x1 < x2) cL

let downS sL pL s =
	let (a,b) = s in
	let cL = b :: List.filter (function p -> upP sL p = Some s)
	in find_ex (function (x1, y1) (x2, y2) -> x1 < x2) cL

let segs sL p =
	let cL = List.sort (function s1 s2 -> (slope s2) - (slope s1))
		(List.filter (function (a,b) -> a = p) sL) and

	t = match downP sL p with
		|None -> []
		|Some s -> [s] in

	match upP sL p with
		|None -> cL @ t
		|Some s -> s::(cL @ t)

(*Main*)
let make_cell sL pL p u v =
	match u, v with
		|(p, pu), (p, pv) ->
			let q = upS sL pL u in
			(match q with
			|snd u -> [p; q; clean (prj v q)]
			|snd v -> [p; clean (prj u q); q]
			| _ -> [p; clean (prj u q); clean (prj v q)])

		|(au, b), (av, b) ->
			let q = upS sL pL u in
			(match q with
			|b -> [q; clean(prj v p); clean (prj u p)]
			| _ -> List.iter clean [prj u p; prj u q; prj v q; prj v p])

		| _ ->
			let q = upS sL pL u in
			List.iter clean [prj u p; prj u q; prj v q; prj v p]

let rec make_cell_L sL pL p = function
	|[] -> []
	|[_] -> []
	|u::(v::tl) -> (make_cell sL pL p u v)::(make_cell_L sL pL p (v::tl))

let do_decomp sL =
	let pL = points_from_segs sL in
	let w = List.map (function p -> (p, segs sL p)) pL in
	List.fold_left (function acc (p, l) -> acc @ (make_cell_L sL pL p l)) [] w
