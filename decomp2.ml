(*Approche légèrement différente des primitives.*)

(*Types*)
type point = {x: float; y: float}
type seg = {sp: point; ep: point}
type open_cell = {st: point; fl: seg; cei: seg; id: int}
type cell = int * (point list)

(*Auxilliaires*)
let names = ref 0

let fresh () =
	names := !names + 1;
	!names

let slope s = (s.ep.y -. s.sp.y)/.(s.ep.x -. s.sp.x)

let same x o = match o with
	|Some y when x = y -> true
	| _ -> false

let rec purge = function
	|[] -> []
	|a::(b::t) when a = b -> purge (a::t)
	|h::t -> h::(purge t)

(*Primitives*)
let prjY s p = (slope s)*.(p.x -. s.sp.x) +. s.sp.y

let prj s p = {x = p.x ; y = prjY s p}

let check s p = (p.x >= s.sp.x) && (p.x <= s.ep.x)

let rec up p = function
	|[] -> None
	|s::t when not (check s p) -> up p t
	|s::t when prjY s p <= p.y -> up p t
	|s::t -> let c = up p t in (
			match c with
			|Some sc when (prjY sc p) < (prjY s p) -> c
			|_ -> Some s)

let rec down p = function
	|[] -> None
	|s::t when not (check s p) -> up p t
	|s::t when prjY s p >= p.y -> up p t
	|s::t -> let c = up p t in (
			match c with
			|Some sc when (prjY sc p) > (prjY s p) -> c
			|_ -> Some s)

(*Transformation des données*)
let rec inser_seg_endpoint s = function
	|[] -> [(s.ep, [], [s])]
	|(p, lA, lB)::t when p.x > s.ep.x -> (s.ep, [], [s])::((p, lA, lB)::t)
	|(p, lA, lB)::t when p.x = s.ep.x -> (p, lA, s::lB)::t
	|h::t -> h::(inser_seg_endpoint s t)

let rec inser_seg s = function
	|[] -> [(s.sp, [s], []); (s.ep, [], [s])]
	|(p, lA, lB)::t when p.x > s.sp.x -> (s.sp, [s], [])::(inser_seg_endpoint s ((p, lA, lB)::t))
	|(p, lA, lB)::t when p.x = s.sp.x -> (p, lA @ [s], lB)::(inser_seg_endpoint s t)
	|h::t -> h::(inser_seg s t)

let slope_compare s s' = match (slope s, slope s') with
	|x, y when x > y -> 1
	|x, y when x < y -> -1
	| _ -> 0

let get_points sL =
	List.fold_left (fun l s -> inser_seg s l) [] (List.sort slope_compare sL)

let rec sep_concerned p d = function
	|[] -> [], []
	|c::t when (same c.fl d)||(c.fl.ep = p)||(c.cei.ep = p)
		-> let lA, lB = sep_concerned p d t in
		lA, c::lB
	|c::t -> let lA, lB = sep_concerned p d t in
		c::lA, lB

let segs_aux u d l =
	(match d with |Some sd -> sd::l |None -> l)@(match u with |Some su -> [su] |None -> [])

let open_new p f c = {st = p; fl = f; cei = c; id = fresh ()}

let rec open_newL p = function
	|[] -> []
	|[x] -> []
	|a::(b::t) -> (open_new p a b)::(open_newL p (b::t))

let close_cell p c = purge [c.st; prj c.fl c.st; prj c.fl p; p; prj c.cei p; prj c.cei c.st; c.st], c.id

let step sL cL pc =
	let p, l_segL, r_segL = pc in
	let u = up p sL and d = down p sL in
	let rem_cell, cells = sep_concerned p d cL and
	new_cells = open_newL p (segs_aux u d r_segL) in
	rem_cell@new_cells, List.map (close_cell p) cells

let rec decomp pcL sL =
	let aux (cL, cellL) pc =
		let cL', c = step sL cL pc in
		(cL', c@cellL) in
	snd (List.fold_left aux ([], []) pcL)